import FastBinaryIndexedTree from 'fast-binary-indexed-tree'

describe('fast-binary-indexed-tree', () => {
    const bit = new FastBinaryIndexedTree({defaultFrequency: 1, maxVal: 10})

    it('should return defaultValue', () => {
        expect(bit.defaultFrequency).toEqual(1)
        expect(bit.maxVal).toEqual(10)
    })
    it('should return value in index', () => {
        expect(bit.readSingle(0)).toEqual(1)
        expect(bit.readSingle(1)).toEqual(1)
    })
    it('should throw when request an index out of index', () => {
        expect(() => bit.readSingle(-1)).toThrow("Index out of range")
        expect(() => bit.readSingle(10)).toThrow("Index out of range")
        expect(() => bit.readSingle(11)).toThrow("Index out of range")
    })

    it('should return sum', () => {
        expect(bit.read(-1)).toEqual(0)
        expect(bit.read(0)).toEqual(0)
        expect(bit.read(1)).toEqual(1)
        expect(bit.read(2)).toEqual(2)
        expect(bit.read(9)).toEqual(9)
        expect(bit.read(10)).toEqual(10)
        expect(bit.read(11)).toEqual(10)
        expect(bit.read(13)).toEqual(10)
    })
    it('should be able to lookup sum', () => {
        expect(bit.lowerBound(-1)).toEqual(0)
        expect(bit.upperBound(-1)).toEqual(0)
        expect(bit.lowerBound(0)).toEqual(0)
        expect(bit.upperBound(0)).toEqual(0)
        expect(bit.lowerBound(0.5)).toEqual(0)
        expect(bit.upperBound(0.5)).toEqual(0)
        expect(bit.lowerBound(1)).toEqual(0)
        expect(bit.upperBound(1)).toEqual(1)
        expect(bit.lowerBound(9)).toEqual(8)
        expect(bit.upperBound(9)).toEqual(9)
        expect(bit.lowerBound(10)).toEqual(9)
        expect(bit.upperBound(10)).toEqual(10)
        expect(bit.lowerBound(11)).toEqual(10)
        expect(bit.upperBound(11)).toEqual(10)
        expect(bit.lowerBound(12)).toEqual(10)
        expect(bit.upperBound(12)).toEqual(10)
    })
})
