declare module 'fast-binary-indexed-tree' {
    class FastBinaryIndexedTree implements ReadonlyFastBinaryIndexedTree {
        readSingle(offset: Offset): Value

        read(offset: Offset): Sum

        lowerBound(sum: Sum): Offset

        upperBound(sum: Sum): Offset

        readonly defaultFrequency: Value
        readonly maxVal: Offset

        update(offset: Offset, value: Value): void

        writeSingle(offset: Offset, value: Value): void

        readSingle(offset: Offset): Value

        read(offset: Offset): Sum

        lowerBound(sum: Sum): Offset

        upperBound(sum: Sum): Offset

        readonly defaultFrequency: Value
        readonly maxVal: Offset

        constructor(prop: { defaultFrequency: Value, maxVal?: Offset })
    }

    type Offset = number
    type Value = number
    type Sum = number

    export = FastBinaryIndexedTree
}

