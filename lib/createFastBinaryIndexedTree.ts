import FastBinaryIndexedTree from 'fast-binary-indexed-tree'

export function createFastBinaryIndexedTree(defaultValue: number,
                                            size: number): FastBinaryIndexedTree {
    return new FastBinaryIndexedTree({defaultFrequency: defaultValue, maxVal: size})
}
