import {ensure} from '@pinyin/maybe'
import {px} from '@pinyin/types'
import FastBinaryIndexedTree from 'fast-binary-indexed-tree'

export function cloneFastBinaryIndexedTree(
    source: FastBinaryIndexedTree,
    newDefaultHeight?: px
): FastBinaryIndexedTree {
    const target = new FastBinaryIndexedTree({
        defaultFrequency: ensure(newDefaultHeight, source.defaultFrequency),
        maxVal: source.maxVal
    });

    (target as any)['_tree'] = {...(source as any)['_tree']};
    (target as any)['_countNeg'] = (source as any)['_countNeg']

    return target
}
