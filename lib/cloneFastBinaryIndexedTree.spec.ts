import FastBinaryIndexedTree from 'fast-binary-indexed-tree'
import {cloneFastBinaryIndexedTree} from './cloneFastBinaryIndexedTree'

describe(`${cloneFastBinaryIndexedTree.name}`, () => {
    it('should have same properties', () => {
        const a = new FastBinaryIndexedTree({
            defaultFrequency: 10,
            maxVal: 33
        })
        const b = cloneFastBinaryIndexedTree(a)
        const c = cloneFastBinaryIndexedTree(a)
        expect(a).toEqual(b)
        expect(a).toEqual(c)

        a.writeSingle(0, 1)
        expect(b).toEqual(c)
    })
})
