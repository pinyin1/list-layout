import {Maybe} from '@pinyin/maybe';
import {px} from '@pinyin/types'
import {ItemID} from './ItemID'
import {ItemSpec} from './ItemSpec'

export interface ListLayout {
    readonly itemIDs: ReadonlyArray<ItemID>
    readonly height: px

    readonly updatedHeights: ReadonlyMap<ItemID, px>

    getByID(id: ItemID): Maybe<ItemSpec>
    getByIndex(index: number): Maybe<ItemSpec>
    getByIndexRange(from: number, to: number): Array<ItemSpec>
    getByOffset(position: px): Maybe<ItemSpec>
    getByOffsetRange(from: px, to: px): Array<ItemSpec>

    updateWith(newShapes: ReadonlyMap<ItemID, px>): ListLayout
    deriveFrom(itemIDs: ReadonlyArray<ItemID>): ListLayout

    clone(): ListLayout
}

