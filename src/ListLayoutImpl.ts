import {assume, ensure, existing, Maybe, notExisting} from '@pinyin/maybe'
import {nothing, px} from '@pinyin/types'
import {cloneFastBinaryIndexedTree} from '../lib/cloneFastBinaryIndexedTree'
import {createFastBinaryIndexedTree} from '../lib/createFastBinaryIndexedTree'
import {ItemID} from './ItemID'
import {ItemSpec} from './ItemSpec'
import {ListLayout} from './ListLayout'
import ReadonlyFastBinaryIndexedTree = require('fast-binary-indexed-tree')
import equal = require('fast-deep-equal')

export class ListLayoutImpl implements ListLayout {
    public get itemIDs(): ReadonlyArray<ItemID> { return this.indexToID }

    public get height(): px {
        return ensure(this.prefixSumIndex.read(this.indexToID.length), 0)
    }

    public get updatedHeights(): ReadonlyMap<ItemID, px> {
        return this.knownHeights
    }

    public getByID(id: ItemID): Maybe<ItemSpec> {
        const {idToIndex, prefixSumIndex} = this
        const index = idToIndex.get(id)
        if (notExisting(index))
            return nothing
        const offset = prefixSumIndex.read(index)
        const height = prefixSumIndex.readSingle(index)
        return {id, index, offset, height}
    }

    public getByIndex(index: number): Maybe<ItemSpec> {
        const {indexToID} = this
        if (!Number.isInteger(index) || index < 0)
            throw new Error(`${index} is not a legal index`)

        const id = indexToID[index]
        if (notExisting(id))
            return nothing
        return this.getByID(id)
    }

    public getByIndexRange(from: number, to: number): Array<ItemSpec> {
        const {indexToID} = this
        from = Math.max(from, 0)
        to = Math.min(to, indexToID.length - 1)

        const results: Array<ItemSpec> = []
        for (let i = from; i <= to; i++) {
            const item = this.getByIndex(i)
            if (notExisting(item))
                break
            results.push(item)
        }
        return results.sort((a, b) => a.index - b.index)
    }

    public getByOffset(position: px): Maybe<ItemSpec> {
        const {prefixSumIndex, indexToID} = this
        let bound = prefixSumIndex.upperBound(position)
        const index = bound === indexToID.length ? bound - 1 : bound
        const id = indexToID[index]
        if (notExisting(id))
            return nothing
        return this.getByID(id)
    }

    public getByOffsetRange(from: px, to: px): Array<ItemSpec> {
        const {height} = this
        from = Math.max(from, 0)
        to = Math.min(to, height - 1)

        const fromItem = this.getByOffset(Math.max(from, 0))
        const toItem = this.getByOffset(Math.min(to, height))
        if (notExisting(fromItem) || notExisting(toItem)) {
            return []
        }
        return this.getByIndexRange(fromItem.index, toItem.index)
    }

    public updateWith(newShapes: ReadonlyMap<ItemID, px>): ListLayout {
        const {idToIndex, knownHeights, prefixSumIndex} = this

        const emptyUpdate = newShapes.size === 0
        const noNewHeight = Array.from(newShapes.entries())
            .every(([id, height]) => knownHeights.get(id) === height)
        if (emptyUpdate || noNewHeight) {
            return this
        }

        const newPrefixSumIndex = cloneFastBinaryIndexedTree(prefixSumIndex)
        const newKnownHeights = new Map<ItemID, px>(knownHeights)
        // TODO do we need to release unused heights?

        newShapes.forEach((height, id) => {
            newKnownHeights.set(id, height)
            assume(idToIndex.get(id), index => newPrefixSumIndex.writeSingle(index, height))
        })

        return this.change({newPrefixSumIndex, newKnownHeights})
    }

    public deriveFrom(itemIDs: ReadonlyArray<ItemID>): ListLayout {
        const {indexToID, defaultHeight, knownHeights, idToIndex} = this

        if (equal(itemIDs, indexToID)) {
            return this
        }

        const newIndexToID = Array.from(indexToID)

        const newIDToIndex = new Map(idToIndex)

        const newPrefixSumIndex = createFastBinaryIndexedTree(defaultHeight, maxSize)
        knownHeights.forEach((height, id) => {
            assume(newIDToIndex.get(id), index => newPrefixSumIndex.writeSingle(index, height))
        })

        return this.change({newIndexToID, newPrefixSumIndex, newIDToIndex})
    }

    public clone(): ListLayout {
        const {indexToID, idToIndex, knownHeights, defaultHeight, prefixSumIndex} = this

        const newPrefixSumIndex = cloneFastBinaryIndexedTree(prefixSumIndex)

        return new ListLayoutImpl(
            defaultHeight,
            indexToID,
            idToIndex,
            knownHeights,
            newPrefixSumIndex
        )
    }

    public static create(itemIDs: ReadonlyArray<ItemID>,
                         defaultHeight: px,
                         predefinedHeights?: ReadonlyMap<ItemID, px>): ListLayout {
        if (itemIDs.length > maxSize) {
            throw new Error(`Item size ${itemIDs.length} exceed maximum size ${maxSize}.`)
        }

        const indexToID = Array.from(itemIDs)
        const idToIndex = itemIDs.reduce(
            (prev, curr, index) => prev.set(curr, index),
            new Map<ItemID, number>()
        )
        const knownHeights = existing(predefinedHeights) ? new Map(predefinedHeights) : new Map()

        const prefixSumIndex = createFastBinaryIndexedTree(defaultHeight, maxSize)

        return new ListLayoutImpl(
            defaultHeight,
            indexToID,
            idToIndex,
            knownHeights,
            prefixSumIndex
        )
    }

    private constructor(
        private readonly defaultHeight: px,
        private readonly indexToID: ReadonlyArray<ItemID>,
        private readonly idToIndex: ReadonlyMap<ItemID, number>,
        private readonly knownHeights: ReadonlyMap<ItemID, px>,
        private readonly prefixSumIndex: ReadonlyFastBinaryIndexedTree
    ) {}

    private change(params: Partial<{
        newDefaultHeight: px,
        newIndexToID: ReadonlyArray<ItemID>,
        newIDToIndex: ReadonlyMap<ItemID, number>,
        newKnownHeights: ReadonlyMap<ItemID, px>,
        newPrefixSumIndex: ReadonlyFastBinaryIndexedTree
    }>): ListLayout {
        const {defaultHeight, indexToID, idToIndex, knownHeights, prefixSumIndex} = this
        const {newDefaultHeight, newIndexToID, newIDToIndex, newKnownHeights, newPrefixSumIndex} = params
        return new ListLayoutImpl(
            ensure(newDefaultHeight, defaultHeight),
            ensure(newIndexToID, indexToID),
            ensure(newIDToIndex, idToIndex),
            ensure(newKnownHeights, knownHeights),
            ensure(newPrefixSumIndex, prefixSumIndex)
        )
    }
}

const maxSize = 1000 // TODO
