import {px} from '@pinyin/types'
import {ItemID} from './ItemID'

export type ItemSpec = {
    id: ItemID
    index: number
    height: px
    offset: px
}
